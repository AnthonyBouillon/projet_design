$(function(){
  // tr cachée qui sert de pattern pour clonage/création d'une nouvelle ligne/article dans la modal panier
  $('#basketModal tbody tr[ref=ref]').hide();
  // array panier
  var panier = [];
  var totalAmount = 0;
  // fonction de calcul et affichage du montant total du panier
  function fullAmount(){
    totalAmount = 0;
    $.each( panier, function( i, val ) {
      console.log(refPrice = parseFloat(val.price));
      console.log(refqt = parseInt(val.qt));
      totalAmount += (refPrice * refqt);
    });
    $('#totalNetPrice').val(totalAmount.toFixed(2));
    $('#basketReminderList li.bg-dark span').html(totalAmount.toFixed(2));
    basketInset();
  }

  // affichage contenu ENCART / recap panier
  function basketInset(){
    if (totalAmount != 0){
      $('#basketReminder .shoppingBasket').show();
      $('#basketReminder .emptyBasket').hide();
    } else {
      $('#basketReminder .emptyBasket').show();
      $('#basketReminder .shoppingBasket').hide();
    }
  }
  basketInset();


  // pour chaque cards=reference d'article, event clic sur le bouton addToBasket 'ajouter au panier'
  $('button[name=addToBasket]').click(function(){
    var ref = $(this).attr('id').toString();
    var articleTitle = $('#title-' + ref).text().toString();
    var description = $('#description-' + ref).text().toString();
    var price = $('#price-' + ref + ' span').text().toString();
    var imgSrc = $('#img-' + ref).attr('src').toString();

    // si objet JS existe on incremente sa quantité
    if(eval(ref).qt  >= 1) {
      eval(ref).qt+=1;
    // sinon creation de l'objet JS  = article ajouté au panier avec ses propriétés. nom de l'objet = ref article
    } else {
      eval(ref + ' ={ ref:ref, titre:articleTitle, img:imgSrc, desc:description, price:price, qt:1};');
      // ajout de l'objet au tableau panier[]
      panier.push(eval(ref));
    }



    // ajout d'une ligne au panier dans encart & modal. OU incrémentation quantité si ref déjà présente dans le panier //
    // index de l'objet-ref{} dans panier[]
    var indexArticle = panier.indexOf(eval(ref));
    if(panier[indexArticle].qt == 1){
      // création list item encart
      // $( "p" ).insertBefore( "#foo" );
      $('<li class="list-group-item" id="quantity' + ref + '"><span class="badge badge-pill pull-left"> ' + panier[indexArticle].qt + ' </span><span class="pull-right"> ' + panier[indexArticle].titre + '</span></li>').insertBefore('#basketReminderList li.bg-dark');

      // creation table row modal
      // clone le tr caché de la modal
      var clone = $('#basketModal tbody tr[ref=ref]').clone();
      clone.attr('ref', ref);
      clone.appendTo($('#basketModal tbody'));
      // remplacement de l'image
      $('#basketModal tbody tr[ref=' + ref + '] img').attr('src', imgSrc);
      // remplacement du titre / nom de l'article
      $('#basketModal tbody tr[ref=' + ref + '] td:nth-child(2)').html(articleTitle);
      // modif des boutons +/- pour changement quantités ds le panier/modal
      $('#basketModal tbody tr[ref=' + ref + '] td form input#minus').attr('ref', ref);
      $('#basketModal tbody tr[ref=' + ref + '] td form input#minus').attr('id', 'minus' + ref);
      $('#basketModal tbody tr[ref=' + ref + '] td form input#plus').attr('ref', ref);
      $('#basketModal tbody tr[ref=' + ref + '] td form input#plus').attr('id', 'plus' + ref);
      // modif quantité
      $('#basketModal tbody tr[ref=' + ref + '] td form input[placeholder=quantite]').attr('id', 'quantity' + ref);;
      $('#basketModal tbody tr[ref=' + ref + '] td form input#quantity' + ref).val(panier[indexArticle].qt);
      // modif bouton 'poubelle' supprimer reference
      $('#basketModal tbody tr[ref=' + ref + '] td button[ref=ref]').attr('ref', ref);
      // modif input total/références + ajout montant total
      $('#basketModal tbody tr[ref=' + ref + '] td form input#totalRefPrice').attr('id', 'totalRefPrice' + ref);
      $('#basketModal tbody tr[ref=' + ref + '] td form input#totalRefPrice' + ref).val(((panier[indexArticle].qt)*(panier[indexArticle].price)).toFixed(2));
      // affichage du nouveau tr modifié
      clone.show();

      // calcul et affichage montant total panier
      fullAmount();

    } else if(panier[indexArticle].qt >1 ){
      // ajout quantité encart
      $('#quantity' + ref + ' span.badge-pill').html(panier[indexArticle].qt);
      // ajout quantité modal
      $('#basketModal tbody tr[ref=' + ref + '] td form input#quantity' + ref).val(panier[indexArticle].qt);
      $('#basketModal tbody tr[ref=' + ref + '] td form input#totalRefPrice' + ref).val(((panier[indexArticle].qt)*(panier[indexArticle].price)).toFixed(2));
      // calcul et affichage montant total panier
      fullAmount();
    }

    // notif confirmation ajout
    $.notify({
      title: '<h4>' + articleTitle + '</h4>',
      message: 'article ajouté à votre panier'
    },{
      type: 'warning',
      placement: {
        from: "top",
        align: "right"
      },
      delay: 500,
      timer: 500,
    });

  });// fin click addToBasket

// boutons +/- pour modif des quantités dans le panier
  $('#basketModal').on('click', 'input[id*=plus]', function(){
    var refQuantity = $(this).attr('ref');
    var indexArticle = panier.indexOf(eval(refQuantity));
    panier[indexArticle].qt++ ;
    // calcul et affichage montant total panier
    fullAmount();
    // ajout quantité et total-reference aux inputs
    $('#basketModal tbody tr[ref=' + refQuantity + '] td form input#quantity' + refQuantity).val(panier[indexArticle].qt);
    $('#basketModal tbody tr[ref=' + refQuantity + '] td form input#totalRefPrice' + refQuantity).val(((panier[indexArticle].qt)*(panier[indexArticle].price)).toFixed(2));
    // ajout quantité encart
    $('#quantity' + refQuantity + ' span.badge-pill').html(panier[indexArticle].qt);
  });
  $('#basketModal').on('click', 'input[id*=minus]', function(){
    var refQuantity = $(this).attr('ref');
    var indexArticle = panier.indexOf(eval(refQuantity));
    if(panier[indexArticle].qt > 1){
      panier[indexArticle].qt-- ;
    }
    // calcul et affichage montant total panier
    fullAmount();
    // ajout quantité et total-reference aux inputs
    $('#basketModal tbody tr[ref=' + refQuantity + '] td form input#quantity' + refQuantity).val(panier[indexArticle].qt);
    $('#basketModal tbody tr[ref=' + refQuantity + '] td form input#totalRefPrice' + refQuantity).val(((panier[indexArticle].qt)*(panier[indexArticle].price)).toFixed(2));
    // ajout quantité encart
    $('#quantity' + refQuantity + ' span.badge-pill').html(panier[indexArticle].qt);
  });
// boutons pour supprimer une reference dans le panier
  $('#basketModal').on('click', 'button.btn-danger', function(){
    console.log(ref);
    var ref = $(this).attr('ref');
    var indexArticle = panier.indexOf(eval(ref));
    panier[indexArticle].qt = 0;
    panier.splice(indexArticle,1);
    $('#basketModal tbody tr[ref=' + ref + ']').remove();
    $('#basketReminderList #quantity' + ref).remove();
    fullAmount();
    if(totalAmount<=0){
      $('#basketModal').modal('toggle');
    }
  });

  $('button#payment').click(function(){
    console.log('merci');
    $.notify({
      title: '<h4>MERCI</h4>',
      message: 'pour votre commande.'
    },{
      type: 'success',
      placement: {
        from: "top",
        align: "center"
      },
      delay: 5000,
      timer: 500,
    });
    });
});
