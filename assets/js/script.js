var store = angular.module('store', ['ngRoute']);

store.run(function($rootScope){
  //rootScope = variable globale qui passe outre les controllers
  //on initialise des tableaux ici pour eviter d'écraser les données au chargement de la page
  // $rootScope.test = 99;
  $rootScope.basket=[];
  $rootScope.basketFilter=[];

});

//configuration des route
store.config(['$routeProvider', function($routeProvider){
  $routeProvider
//quand je click sur form je vais chercher mon fichier form.html et utilise mon controller formControl
  .when('/home', {
    templateUrl:'partials/home.html',
    controller:'homeControl'
  })
  // ? veut dire que c'est optionnel
  .when('/categories/:ref?', {
    templateUrl:'partials/categories.html',
    controller:'categoriesControl'
  })
  .when('/articles/:ref?', {
    templateUrl:'partials/articles.html',
    controller:'articlesControl'
  })
  .when('/basket/:ref?', {
    templateUrl:'partials/basket.html',
    controller:'basketControl'
  })
  //sinon j'utilise par default form
  .otherwise({
    redirectTo:'/home'
  });
}]);


store.controller('categoriesControl', ['$scope', '$routeParams', '$rootScope', '$http', function($scope, $routeParams, $rootScope, $http){
  $http.get("assets/js/articles.json").then(function(response) {
      $scope.articles = response.data;
  });
  //routeParams récupère toutes les variables de ref
    $scope.ref=$routeParams.ref;
    //boutons ajout au panier
    $scope.addToBasket = function(article, qt) {

      basketEntry={ ref:article, quantity:qt };
     console.log("quantityOf" + article) ;


      if($rootScope.basket[0]==undefined){
        console.log($rootScope.basket[0]);
        $rootScope.basket.push(basketEntry);
        $rootScope.basketFilter.push(article); //array provisoire pour filtre ng-repeat affichage panier

      } else{
        //jquery
        flag=false;
        $.each($rootScope.basket, function (i, item) {

          if(item.ref==basketEntry.ref){
            console.log('present ' + basketEntry.ref);
            item.quantity += 1;
            console.log('qt++ ' + item.quantity);
            flag = true;
          } else if($rootScope.basket.length==(i+1) && flag==false){
            console.log('pushing...');
            $rootScope.basket.push(basketEntry);
            $rootScope.basketFilter.push(article);
          }
        });// fin jquery
      }
      console.log($rootScope.basket);
      $.notify({
        title: '<h4>titre</h4>',
        message: 'article ajouté à votre panier'
      },{
        type: 'warning',
        placement: {
          from: "top",
          align: "right"
        },
        delay: 1000,
        timer: 500,
      });
    }


  }]);//end categoriesControl


store.controller('articlesControl', ['$scope', '$routeParams', '$rootScope', '$http', function($scope, $routeParams, $rootScope, $http){
  $http.get("assets/js/articles.json").then(function(response) {
      $scope.articles = response.data;
  });
  //routeParams récupère toutes les variables de id
    $scope.ref=$routeParams.ref;
}]);//end articlesControl


store.controller("basketControl", ['$scope', '$routeParams', '$rootScope', '$http', function($scope, $routeParams, $rootScope, $http){
//recup fichier json dans articles
  $http.get('assets/js/test.json')
          .then(function(res){
             $scope.articles = res.data;
           });
  //filtre ngrepeat affichage modal panier
  $scope.filterByBasket = function(article) {
      return ($rootScope.basketFilter.indexOf(article.ref) !== -1);
      };
// boutons supprimer ref/articles du panier
  $scope.removeFromBasket = function(ref) {
    $scope.id = $rootScope.basketFilter.indexOf(ref);
    console.log($scope.id);
    $rootScope.basketFilter.splice($scope.id, 1);
    $rootScope.basket.splice($scope.id, 1);
      }
$scope.test = $rootScope.test;

}]); //end basketControl


store.controller("homeControl", function($scope, $http) {

  $http.get('assets/js/articles.json')

        .then(function(res){

           $scope.articles = res.data;

         });

});

// Test retour en haut

// window.onscroll = function() {scrollFunction()};
//
// function scrollFunction() {
//
//     if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
//
//         document.getElementById("myBtn").style.display = "block";
//
//     } else {
//
//         document.getElementById("myBtn").style.display = "none";
//
//     }
//
//
// }
